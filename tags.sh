#!/bin/bash
ctags --recurse * /lib/modules/`uname -r`/source/{include,kernel,lib,drivers/usb,drivers/media/video}
ctags -e --recurse * /lib/modules/`uname -r`/source/{include,kernel,lib,drivers/usb,drivers/media/video}
