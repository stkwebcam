KVER ?= $(shell uname -r)
KSRC ?= /lib/modules/$(KVER)/build


all: driver

driver:
	$(MAKE) -C "$(KSRC)" SUBDIRS="$(PWD)" modules
	@ctags -R

clean:
	$(MAKE) -C "$(KSRC)" SUBDIRS="$(PWD)" clean
	@rm -f Module.symvers

